<?php
/**
 * Author   : nicolas.glassey@cpnv.ch
 * Project  : PhpExceptionManagement
 * Created  : 03.06.2019 - 00:04
 *
 * Last update :    [01.12.2018 author]
 *                  [add $logName in function setFullPath]
 * Git source  :    [link]
 */

require "controler/controler.php";

if (isset($_GET['action'])) {
    $action = $_GET['action'];
    switch ($action) {
        case 'calcRequest' :
            division($_POST);
            break;
        default :
            division(null);
    }
}
else {
    division(null);
}