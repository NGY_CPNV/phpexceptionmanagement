<?php
/**
 * Author   : nicolas.glassey@cpnv.ch
 * Project  : PhpExceptionManagement
 * Created  : 02.06.2019 - 23:19
 *
 * Last update :    [01.12.2018 author]
 *                  [add BadOperatorException in function setFullPath]
 * Git source  :    [link]
 */

class BadOperatorException extends Exception
{
    protected $message = "Operator used is not implemented";
}