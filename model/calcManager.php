<?php
/**
 * Author   : nicolas.glassey@cpnv.ch
 * Project  : PhpExceptionManagement
 * Created  : 03.06.2019 - 00:03
 *
 * Last update :    [01.12.2018 author]
 *                  [add $logName in function setFullPath]
 * Git source  :    [link]
 */


require_once "BadOperatorException.php";

/**
 * This function is designed to execute a basic calculation based on 2 operandes and one operator
 * @param $op1 : first operand
 * @param $operator : operator used
 * @param $op2 : second operand
 * @return float|int|null       : result can be float (result of a division), integer or null (exception)
 * @throws BadOperatorException : in case of the operator is not recognized by the function
 * @throws DivisionByZeroError  : in case of the second operator in a division operation is zero
 */
function calc($op1, $operator, $op2)
{
    $result = null;
    switch ($operator) {
        case "/":
            if ($op2 == 0) {
                throw new DivisionByZeroError;
            }
            $result = $op1 / $op2;
            break;
        default:
            throw new BadOperatorException;
            break;
    }
    return $result;
}
