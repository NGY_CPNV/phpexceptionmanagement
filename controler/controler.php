<?php
/**
 * Author   : nicolas.glassey@cpnv.ch
 * Project  : PhpExceptionManagement
 * Created  : 02.06.2019 - 23:45
 *
 * Last update :    [01.12.2018 author]
 *                  [add $logName in function setFullPath]
 * Git source  :    [link]
 */

require_once "model/calcManager.php";
$errorMsg = "";

function division($divisionParameters)
{
    if (isset($divisionParameters)) {
        try {
            $result = calc($divisionParameters['op1'], $divisionParameters['operator'], $divisionParameters['op2']);
            $errorMsg = "";
        } catch (DivisionByZeroError $e) {
            //https://www.php.net/manual/en/class.divisionbyzeroerror.php
            $errorMsg = "Vous ne pouvez pas effectuer une division par zéro !";
            //$e->getMessage() can be written in a log file
        } catch (BadOperatorException $e) {
            $errorMsg = "L'opérateur utilisé n'est pas supporté";
            //$e->getMessage() can be written in a log file
        }
    }
    require_once "view/calcRequest.php";
}
?>


