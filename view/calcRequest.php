<?php
/**
 * Author   : nicolas.glassey@cpnv.ch
 * Project  : PhpExceptionManagement
 * Created  : 03.06.2019 - 00:08
 *
 * Last update :    [01.12.2018 author]
 *                  [add $logName in function setFullPath]
 * Git source  :    [link]
 */
if (!isset($errorMsg)){
    $errorMsg = "";
}
?>

<!DOCTYPE html>
<html lang="en">
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <link rel="stylesheet" href="content/css/bootstrap.min.css">
</head>
<body>
<?php if($errorMsg != "") : ?>
    <?php echo ($errorMsg) ?>
<?php endif; ?>
<form method="post" action="index.php?action=calcRequest">
    Opérande 1 : <input type="number" name="op1" required><br>
    Opérateur  : <input type="text" name="operator" required><br>
    Opérande 2 : <input type="number" name="op2" required><br>
    <input type="submit">
</form>
</body>
</html>